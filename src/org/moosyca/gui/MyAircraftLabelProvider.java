package org.moosyca.gui;

import java.net.URL;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
//import org.eclipse.ui.ISharedImages;
//import org.eclipse.ui.PlatformUI;
import org.moosyca.gui.MyAircraft;
import org.moosyca.gui.MyWorkSession;
import org.moosyca.gui.MyAeroComponent;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

public class MyAircraftLabelProvider extends LabelProvider 
{

	private static final Image FOLDER_IMG = getImage("Folder_32x32.png");
	private static final Image FOLDER_AIRCRAFT_IMG = getImage("FolderAirplane_32x32.png");
	private static final Image AEROCOMPONENT_IMG = getImage("AirplanePaper_32x32.png");
	private static final Image ECLIPSE_IMG = getImage("eclipse32.png");

	@Override
	public String getText(Object element) {
		if (element instanceof MyAircraft) {
			MyAircraft ac = (MyAircraft) element;
			return ac.getName();
		}
		if (element instanceof MyAeroComponent) {
			MyAeroComponent aercomp = (MyAeroComponent) element;
			return aercomp.getName();
		}
		return "null";
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof MyAircraft) {
			return FOLDER_AIRCRAFT_IMG;
		}
		if (element instanceof MyAeroComponent) {
			return AEROCOMPONENT_IMG;
		}
		return ECLIPSE_IMG;
	}

	// Helper Method to load the images
	private static Image getImage(String file) {
		ImageDescriptor image = 
				ImageDescriptor.createFromFile(
						MOOSyCA_GUI.class,
						"images/" + file
						);
		return image.createImage();
	} 
}
