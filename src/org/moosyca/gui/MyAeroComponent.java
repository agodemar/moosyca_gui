package org.moosyca.gui;

import org.eclipse.swt.graphics.Image;

public class MyAeroComponent {

	public static final int UNDEFINED             = -1;
	public static final int MAIN_LIFTING_SURFACE  = 0;
	public static final int HORIZONTAL_TAIL       = 1;
	public static final int VERTICAL_TAIL         = 2;
	public static final int CANARD                = 3;
	public static final int MAIN_BODY             = 4;
	public static final int NACELLE               = 5;
	
	private String _name = "";
	private String _description = "";
	private int _type = -1;

	public MyAeroComponent(String name, int type) {
		this._name = name;
		this._type = type;
	}

	public MyAeroComponent(String name, int type, String description) {
		this._name = name;
		this._type = type;
		this._description = description;
	}
	
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}

	public int getType() {
		return _type;
	}
	public void setType(int type) {
		this._type = type;
	}

	public String getDescription() {
		return _description;
	}
	public void setDescription(String description) {
		this._description = description;
	}

}