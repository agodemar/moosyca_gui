package org.moosyca.gui;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import org.moosyca.gui.MyAircraft;
import org.moosyca.gui.MyWorkSession;
import java.util.ArrayList;
import java.util.List;

public class MyAircraftContentProvider implements ITreeContentProvider {

	private MyWorkSession _workSession;
	private MyAircraft _aircraft;

	public MyAircraftContentProvider(MyWorkSession ws, MyAircraft ac)
	{
		super();
		_workSession = ws;
		_aircraft = ac;
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this._aircraft = (MyAircraft) newInput;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return _aircraft.getAeroComponents().toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof MyAircraft) {
			MyAircraft ac = (MyAircraft) parentElement;
			return ac.getAeroComponents().toArray();
		}
		return null;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof MyAircraft) {
			return true;
		}
		return false;
	}	  
}
