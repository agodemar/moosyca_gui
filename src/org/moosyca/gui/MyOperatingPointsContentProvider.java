package org.moosyca.gui;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class MyOperatingPointsContentProvider implements ITreeContentProvider {

 	private MyOperatingPoints _operatingPoints;

//	private MyWorkSession _workSession;
//	private MyAircraft _aircraft;
//
//	public MyOperatingPointsContentProvider(MyWorkSession ws, MyAircraft ac)
//	{
//		super();
//		_workSession = ws;
//		_aircraft = ac;
//	}

	@Override
	public void dispose() {
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		this._operatingPoints = (MyOperatingPoints) newInput;
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return _operatingPoints.getOperatingPointsList().toArray();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof MyOperatingPoints) {
			MyOperatingPoints ops = (MyOperatingPoints) parentElement;
			return ops.getOperatingPointsList().toArray();
		}
		return null;
	}

	@Override
	public Object getParent(Object element) {
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		if (element instanceof MyOperatingPoints) {
			return true;
		}
		return false;
	}	  
}