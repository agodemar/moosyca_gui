package org.moosyca.gui;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;

public class MyStatusAction extends Action
{
	StatusLineManager statman;
	short triggercount = 0;
	
	public MyStatusAction(StatusLineManager sm)
	{
		super("&Trigger@Ctrl+T", AS_PUSH_BUTTON);
		
		statman = sm;
		setToolTipText("Trigger the Action");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/eclipse32.png")
				);

	}
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The status action has fired. Count: " +
				triggercount
				);
		
		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The status action has fired. Count: " +
				triggercount + "\n"
				);
	}
}