package org.moosyca.gui;

import java.util.ArrayList;
import java.util.List;

public class MyAircraft {

	private MyWorkSession _workSession = null;
	private String _name;
	private List<MyAeroComponent> _aeroComponents = new ArrayList<MyAeroComponent>();

	public MyAircraft() {
	}
	public MyAircraft(String name) {
		this._name = name;
	}
	
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}

	public void addAerocomponent(MyAeroComponent aercomp) {
		this._aeroComponents.add(aercomp);
	}
	public void removeAerocomponent(MyAeroComponent aercomp) {
		this._aeroComponents.remove(aercomp);
	}
	public void resetAerocomponent() {
		this._aeroComponents.clear();
	}
	public List<MyAeroComponent> getAeroComponents() {
		return _aeroComponents;
	}
	
	public void setParentWorkSession(MyWorkSession s)
	{
		_workSession = s;
	}

}
