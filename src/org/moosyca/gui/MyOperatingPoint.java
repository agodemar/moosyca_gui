package org.moosyca.gui;

public class MyOperatingPoint {

	private String _name = "";
	private String _description = "";
	private double _Mach = 0.2;

	public MyOperatingPoint(String name) {
		this._name = name;
	}

	public MyOperatingPoint(String name, String description) {
		this._name = name;
		this._description = description;

	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		this._name = name;
	}

	public String getDescription() {
		return _description;
	}

	public void setDescription(String description) {
		this._description = description;
	}
	
	public void setMach(double mach) {
		this._Mach = mach;
	}

	public double getMach() {
		return this._Mach;
	}

}
