package org.moosyca.gui;

import java.util.ArrayList;
import java.util.List;

public class MyOperatingPoints {

	private List<MyOperatingPoint> _operatingPoints = new ArrayList<MyOperatingPoint>();	

	public MyOperatingPoints() {
	}
	public MyOperatingPoints(List<MyOperatingPoint> opl) {
		_operatingPoints = opl;
	}
	
	public void addOperatingPoint(MyOperatingPoint op)
	{
		_operatingPoints.add(op);
	}
	public void removeOperatingPoint(MyOperatingPoint op)
	{
		_operatingPoints.remove(op);
	}
	public void resetOperatingPoint()
	{
		_operatingPoints.clear();
	}
	
	public List<MyOperatingPoint> getOperatingPointsList()
	{
		return _operatingPoints;
	}
	public Object[] getOperatingPointsArray()
	{
		return _operatingPoints.toArray();
	}
}
