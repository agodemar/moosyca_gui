package org.moosyca.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

/**
 * Implements a scene grip, capable of rotating and moving a GL scene with
 * the help of the mouse and keyboard.
 * 
 * @author Bo Majewski
 */
public class MyOpenGLSceneGrip extends MouseAdapter implements MouseMoveListener,
		Listener, KeyListener {
	
	private int _canvasWidth = 0;
	private int _canvasHeight = 0;
	private GLCanvas _canvas;
	private MyOpenGLContainer _container;

	private float _xrot;
	private float _yrot;
	private float _zoff;
	private float _xoff;
	private float _yoff;
	private float _xcpy;
	private float _ycpy;
	private float _xscale;
	private float _yscale;
	private boolean _move;
	private int _xdown;
	private int _ydown;
	private int _mouseDown;

	public MyOpenGLSceneGrip(final MyOpenGLContainer myOpenGLContainer) {
		this._container = myOpenGLContainer;
		this._canvas = myOpenGLContainer.getGLCanvas();
		this.init();
	}

	protected void init() {
		_canvas.setCurrent();
		Rectangle bounds = _canvas.getBounds();
		_canvasWidth = bounds.width;
		_canvasHeight = bounds.height;
		this._xrot = this._yrot = 0.0f;
		this._xoff = this._yoff = 0.0f;
		_xscale = 1.0f;
		_yscale = 1.0f;
		_zoff = 1.0f;
		if (_xscale < _yscale)
			_zoff = _xscale;
		else
			_zoff = _yscale;
		
//		// log message
//		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
//				"SCENE GRIP init: \n" + 
//				"\txscale = " + _xscale + " yscale = " + _yscale + " zoff = " + _zoff
//				);
	}

	public void mouseDown(MouseEvent e) {
		if (++this._mouseDown == 1) {
			if ((this._move = e.button == 3)) {
				this._xcpy = _xoff;
				this._ycpy = _yoff;
				((Control) e.widget).setCursor(e.widget.getDisplay()
						.getSystemCursor(SWT.CURSOR_HAND));
			} else {
				this._xcpy = _xrot;
				this._ycpy = _yrot;
				((Control) e.widget).setCursor(e.widget.getDisplay()
						.getSystemCursor(SWT.CURSOR_SIZEALL));
			}

			this._xdown = e.x;
			this._ydown = e.y;
		}
	}

	public void mouseUp(MouseEvent e) {
		if (--this._mouseDown == 0) {
			((Control) e.widget).setCursor(e.widget.getDisplay()
					.getSystemCursor(SWT.CURSOR_ARROW));
		}
	}

	public void mouseMove(MouseEvent e) {
		Point p = ((Control) e.widget).getSize();

		if (this._mouseDown > 0) {
			
			int dx = e.x - this._xdown;
			int dy = e.y - this._ydown;

			this._container.getCamera().setRotation(
					this._container.getCamera().pitch() + dy*0.01f, 
					this._container.getCamera().yaw(), 
					this._container.getCamera().roll()
					);
			
			if (this._move) {
				_yoff = this._ycpy + ((_canvasHeight / 2.f) * dy)
						/ (2.0f * p.y);
				_xoff = this._xcpy + ((_canvasWidth / 2.f) * dx)
						/ (2.0f * p.x);
			} else {
				_xrot = this._xcpy + dy / 2.0f;
				_yrot = this._ycpy + dx / 2.0f;
			}
		}
	}

	public void handleEvent(Event event) {
		this._zoff *= 1.1f;
	}
	
	public void zoomIn() {
		this._zoff *= 1.05f;
	}
	public void zoomOut() {
		this._zoff *= .95f;
	}
	
	public void rotate(final int direction) {
		switch(direction) {
		case SWT.ARROW_UP:
			this._xrot -= 5f;

			break;
		case SWT.ARROW_DOWN:
			this._xrot += 5f;

			break;
		case SWT.ARROW_LEFT:
			this._yrot += 5f;

			break;
		case SWT.ARROW_RIGHT:
			this._yrot -= 5f;
			break;
		default:
			return;
		}
	}
	
	public void translate(final int direction) {
		switch (direction) {
		case SWT.ARROW_UP:
			this._yoff -= (float) _canvasHeight / 10.f;
			break;
		case SWT.ARROW_DOWN:
			this._yoff += (float) _canvasHeight / 10.f;
			break;
		case SWT.ARROW_LEFT:
			this._xoff -= (float) _canvasWidth / 10.f;
			break;
		case SWT.ARROW_RIGHT:
			this._xoff += (float) _canvasWidth / 10;
			break;
		default:
			return;
		}
	}

	public void keyPressed(KeyEvent e) {
		
		if ((e.stateMask & SWT.CONTROL) != 0) {
			rotate(e.keyCode);
			return;
		}

		translate(e.keyCode);
		switch (e.keyCode) {
		
		case SWT.PAGE_UP:
			zoomIn();
			break;
		case SWT.PAGE_DOWN:
			zoomOut();
			break;
		case SWT.HOME:
			this.init();
			break;
		}
	}

	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Warning called constantly in display loop - change with care.
	 */
	public void adjust() {
		_canvas.setCurrent();
		try {
			GLContext.useContext(_canvas);
		} catch (LWJGLException ex) {
			showMessage("Error in adjust using GLContext.useContext\n" + ex);
		}
		// gl = context.getGL ();
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		/* set the orthogonal projection to the size of the window */
		GL11.glOrtho(0, _canvasWidth, _canvasHeight, 0, -1.0e5f, 1.0e5f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		GL11.glTranslatef(_canvasWidth / 2 + this._xoff, _canvasHeight / 2
				+ this._yoff, 0);
		GL11.glScalef(_zoff, _zoff, _zoff);
		/*
		 * _zoff has no effect on the orthogonal projection therefore zoom by
		 * passing _zoff to scale
		 */
		GL11.glRotatef(this._xrot, 1f, 0.0f, 0.0f);
		GL11.glRotatef(this._yrot, 0.0f, 1f, 0.0f);
		//GL11.glTranslatef(-prov.getImageWidth() / 2, -prov.getImageHeight() / 2, 0);
	}

	public void setOffsets(float x, float y, float z) {
		this._xoff = x;
		this._yoff = y;
		this._zoff = z;
	}

	public void setRotation(float x, float y) {
		this._xrot = x;
		this._yrot = y;
	}

	public void setBounds(Rectangle bounds) {
		_canvasWidth = bounds.width;
		_canvasHeight = bounds.height;
	}

	/**  
	 * Shows an error  
	 *   
	 * @param s the error to show  
	 */   
	private void showMessage(String s) {   
		MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_WORKING | SWT.OK);   
		mb.setText("Yes!");
		mb.setMessage(s);   
		mb.open();   
	}   


}// end-of-class definition