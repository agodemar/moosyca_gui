package org.moosyca.gui;

import java.awt.Component;

import javax.swing.JFrame;

import org.eclipse.swt.*;
import org.eclipse.swt.awt.SWT_AWT;

import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.opengl.GLData;
import org.eclipse.swt.widgets.Composite;

import de.jreality.math.MatrixBuilder;
import de.jreality.scene.Appearance;
import de.jreality.scene.Camera;
import de.jreality.scene.DirectionalLight;
import de.jreality.scene.Light;
import de.jreality.scene.SceneGraphComponent;
import de.jreality.scene.SceneGraphPath;
import de.jreality.scene.Viewer;
import de.jreality.swing.JFakeFrame;
import de.jreality.tools.DraggingTool;
import de.jreality.tools.EncompassTool;
import de.jreality.tools.RotateTool;
import de.jreality.tools.ShipNavigationTool;
import de.jreality.toolsystem.ToolSystem;
import de.jreality.util.RenderTrigger;

public class MyJOGLContainer extends Composite
{

	private final Composite _theComposite; 
	
	MyJOGLContainer(Composite parent, int style) 
	{			
		super(parent, style | SWT.EMBEDDED);
		
		_theComposite = this;
		
	    // create scene
	    
	    SceneGraphComponent rootNode=new SceneGraphComponent();
	    SceneGraphComponent geometryNode=new SceneGraphComponent();
	    SceneGraphComponent cameraNode=new SceneGraphComponent();
	    SceneGraphComponent lightNode=new SceneGraphComponent();
	    
	    rootNode.addChild(geometryNode);
	    rootNode.addChild(cameraNode);
	    cameraNode.addChild(lightNode);
	    
	    final MyCatenoidHelicoid geom=new MyCatenoidHelicoid(50);
	    geom.setAlpha(Math.PI/2.-0.3);
	    
	    Camera camera=new Camera();
	    Light light=new DirectionalLight();
	    
	    geometryNode.setGeometry(geom);
	    cameraNode.setCamera(camera);
	    lightNode.setLight(light);

	    Appearance app=new Appearance();
	    //app.setAttribute(CommonAttributes.FACE_DRAW, false);
	    //app.setAttribute("diffuseColor", Color.red);
	    //app.setAttribute(CommonAttributes.TRANSPARENCY_ENABLED, true);
	    //app.setAttribute(CommonAttributes.TRANSPARENCY, 0.4);
	    //app.setAttribute(CommonAttributes.BACKGROUND_COLOR, Color.blue);
	    rootNode.setAppearance(app);
	    
	    MatrixBuilder.euclidean().rotateY(Math.PI/6).assignTo(geometryNode);
	    MatrixBuilder.euclidean().translate(0, 0, 12).assignTo(cameraNode);
	    MatrixBuilder.euclidean().rotate(-Math.PI/4, 1, 1, 0).assignTo(lightNode);

	    SceneGraphPath cameraPath=new SceneGraphPath();
	    cameraPath.push(rootNode);
	    cameraPath.push(cameraNode);
	    cameraPath.push(camera);
	    
	    Viewer viewer = null;
	    ToolSystem toolSystem;
	    
//	    this.setLayout(new FillLayout());
//	    GLData data = new GLData ();
//	    data.doubleBuffer = true;
//	    System.out.println("data.depthSize="+data.depthSize);
//		  data.depthSize = 8;
	    

	    // true for SWT, false for AWT
	    if (false) {
	      // create Shell, GLCanvas and SwtViewer 
	      SwtQueue f = SwtQueue.getInstance();
	      //final Shell shell = f.createShell();
	      final GLCanvas[] theCanvas = new GLCanvas[1];

		  GLData data = new GLData ();
		  data.doubleBuffer = true;
		  System.out.println("data.depthSize="+data.depthSize);
		  data.depthSize = 8;
		  theCanvas[0] = new GLCanvas(this, SWT.NONE, data);
		  theCanvas[0].setCurrent();	    				  
	      
		  /*
	      f.waitFor(new Runnable() {
	    	  public void run() {
	    		  try {
	    			  //Composite composite = MOOSyCA_GUI.getApp().getTopLevelComposite().getMyTabbedPane().getJOGLContainer();
	    			  if ( MOOSyCA_GUI.getApp().getTopLevelComposite() != null )
	    			  {
//	    				  Composite composite = MOOSyCA_GUI.getApp().getTopLevelComposite().getMyTabbedPane().getJOGLContainer();		    			  composite.setLayout(new FillLayout());
//		    			  GLData data = new GLData ();
//		    			  data.doubleBuffer = true;
//		    			  System.out.println("data.depthSize="+data.depthSize);
//		    			  data.depthSize = 8;
//		    			  theCanvas[0] = new GLCanvas(composite, SWT.NONE, data);
//		    			  theCanvas[0].setCurrent();	    				  
	    			  }
	    		  } catch (Exception e) {
	    			  e.printStackTrace();
	    		  }
	    	  }
	      });
	      */
	      final SwtViewer swtViewer=new SwtViewer(theCanvas[0]);
	      
	      // enable tools
//	      viewer = new ToolSystemViewer(swtViewer);
	      toolSystem = ToolSystem.toolSystemForViewer(swtViewer);
	      toolSystem.initializeSceneTools();
	    }
	    else {
	      viewer = new de.jreality.jogl.JOGLViewer();
	      // JFrame f = new JFrame("AWT");
	      
	      java.awt.Frame f = SWT_AWT.new_Frame(this);
	      
	      java.awt.Panel panel = new java.awt.Panel(new java.awt.BorderLayout());
	      f.add(panel);
	      //f.setSize(640, 480);
	      
	      //f.getContentPane().add((Component) viewer.getViewingComponent());
	      f.add((Component) viewer.getViewingComponent());
	      
	      f.setVisible(true);
	      //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    }
	    
	    viewer.setSceneRoot(rootNode);
	    viewer.setCameraPath(cameraPath);

	    toolSystem = ToolSystem.toolSystemForViewer(viewer);
	    toolSystem.initializeSceneTools();
//	    viewer.initializeTools();
	    
	    // add tools
	    geometryNode.addTool(new RotateTool());
	    geometryNode.addTool(new DraggingTool());
	    geometryNode.addTool(new EncompassTool());
	    
	    ShipNavigationTool shipNavigationTool = new ShipNavigationTool();
	    shipNavigationTool.setGravity(0);
	    cameraNode.addTool(shipNavigationTool);
	    
	    MyPaintComponent pc = new MyPaintComponent();
	    JFakeFrame jrj = new JFakeFrame();
	    jrj.getContentPane().add(pc);
	    
	    geometryNode.setAppearance(jrj.getAppearance());
	    geometryNode.addTool(jrj.getTool());
	    
	    // add a render trigger for auto redraw
	    RenderTrigger rt = new RenderTrigger();
	    rt.addViewer(viewer);
	    rt.addSceneGraphComponent(rootNode);



	
	}

}
