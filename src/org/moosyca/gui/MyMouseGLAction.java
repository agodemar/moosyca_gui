package org.moosyca.gui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Display;

public class MyMouseGLAction extends Action
{

	StatusLineManager _statusLineManager;
	short _triggerCount = 0;
	
	public MyMouseGLAction(StatusLineManager sm)
	{
		super("Mouse click", IAction.AS_PUSH_BUTTON);
		
		_statusLineManager = sm;
		setToolTipText("Mouse click in GL canvas");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/Options_32x32.png")
				);
	}
	
	public void run()
	{
		_triggerCount++;
		_statusLineManager.setMessage(
				"The option action has fired. Count: " +
				_triggerCount
				);
		
		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"Mouse action has fired. Count: " +
				_triggerCount + "\n"
				);

		MessageDialog.openInformation(Display.getCurrent().getActiveShell(),
				"Click!", "You clicked me!");

		
		
	}// end of run method
	
}// end of class definition 