package org.moosyca.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.custom.SashForm;

import java.util.Calendar;
import java.text.SimpleDateFormat;
import org.eclipse.swt.widgets.DateTime;

public class MyTopLevelComposite extends Composite
{
	
	// private static final Logger LOGGER = Logger.getLogger(MyTopLevelComposite.class);
	
	private MyProjectPane _projectPane = null;
	
	private MyTabbedPane _tabbedPane = null;
	public MyTabbedPane getMyTabbedPane() { return _tabbedPane;};
	
	private TabFolder _tabFolderLog = null;
	private TabItem _tabItemLog = null;
	private Text _textLogger = null;
	private SashForm _sashFormUpDw = null;
	private SashForm _sashFormLeftRight = null;
	
	private static boolean _connectedToMatlab = false;

	public MyTopLevelComposite(Composite parent, int style) {
		
		super(parent, style);
		
		// Title of the GUI
		getShell().setText("MOOSyCA");
		
		this.setLayout(new FillLayout());

		//--------------------------------------------------------------------
		// Horizontal division (Up/Down)
		
		_sashFormUpDw = new SashForm(this, SWT.VERTICAL);
		
		// Change the width of the sashes
		//sashFormUpDw.SASH_WIDTH = 10;
		// Set the relative weights
		//sashFormUpDw.setWeights(new int[] { 1, 3});
		// Change the color used to paint the sashes
		_sashFormUpDw.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));

		//--------------------------------------------------------------------
		// Upper sash for Left/Right division
		
		_sashFormLeftRight = new SashForm(_sashFormUpDw, SWT.HORIZONTAL|SWT.BORDER);
		
		// Change the color used to paint the sashes
		_sashFormLeftRight.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));
		
		// Left pane --> project view
		_projectPane = new MyProjectPane(_sashFormLeftRight,SWT.NONE);
		// Right pane --> tabs
		_tabbedPane = new MyTabbedPane(_sashFormLeftRight,SWT.NONE);
		
		//--------------------------------------------------------------------
		// The text window for message logs

		_tabFolderLog = new TabFolder(_sashFormUpDw, SWT.NONE);
		_tabItemLog = new TabItem(_tabFolderLog, SWT.NULL);
		_tabItemLog.setText("Log Messages");
		_textLogger = 
				new Text(_tabFolderLog, 
						SWT.MULTI
						| SWT.BORDER
						| SWT.H_SCROLL
						| SWT.V_SCROLL
						);
		_tabItemLog.setControl(_textLogger);

		// Initial message

		DateTime dateTimeGUI = new DateTime(parent, SWT.DATE | SWT.TIME);
		Calendar instance = Calendar.getInstance();
		instance.set(Calendar.DAY_OF_MONTH, dateTimeGUI.getDay());
		instance.set(Calendar.MONTH, dateTimeGUI.getMonth());
		instance.set(Calendar.YEAR, dateTimeGUI.getYear());
		String dateString = 
				new SimpleDateFormat("dd MMM yyyy -- kk:mm:ss (zzz)").format(instance.getTime());
        
		_textLogger.setText(
				"MOOSyCA_GUI -- " + dateString + "\n***\n");
		
		// Set the relative weights
		// NOTE: set them here when layout is done
		_sashFormUpDw.setWeights(new int[] { 5, 1});
		_sashFormLeftRight.setWeights(new int[] { 1, 4});

//		sashFormLeftRight.pack();
//		sashFormUpDw.pack();

	}

	public Text getMessageTextWindow()
	{
		return _textLogger;
	}
	
	public SashForm getSashFormUpDw()
	{
		return _sashFormUpDw;
	}
	public SashForm getSashFormLeftRight()
	{
		return _sashFormLeftRight;
	}

	public boolean isConnectedToMatlab() { return _connectedToMatlab; }
	public void setConnectedToMatlab(boolean yesOrNot) { _connectedToMatlab = yesOrNot; }
	public void toggleConnectionStatus() { _connectedToMatlab = !_connectedToMatlab; }

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}