package org.moosyca.gui;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;
import org.eclipse.swt.widgets.Display;

public class MyXMLViewAction extends Action
{
	StatusLineManager _statman;
	short _triggercount = 0;
	
	public MyXMLViewAction(StatusLineManager sm)
	{
		super("&View XML ...@Ctrl+L", AS_PUSH_BUTTON);
		
		_statman = sm;
		setToolTipText("Explore XML structure");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/XML_32x32.png")
				);
	}
	public void run()
	{
		_triggercount++;
		_statman.setMessage(
				"The XML view action has fired. Count: " +
				_triggercount
				);
		
		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The XML view action has fired. Count: " +
				_triggercount + "\n"
				);
		
		// Launch file chooser window here
		// get the file and show a dialog
		// ...
		// Modal widget
		MyXMLViewDialog dialog = 
				new MyXMLViewDialog(
						Display.getCurrent().getActiveShell(),
						MOOSyCA_GUI.getApp().getTopLevelComposite()
						);
		dialog.open();

		
	}// end of run method
	
}// end of class definition 