package org.moosyca.gui;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;

import org.eclipse.jface.action.*;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import matlabcontrol.PermissiveSecurityManager;

public class MOOSyCA_GUI extends ApplicationWindow {

	/** The path to the directory containing the images */   
	public static final String IMAGE_PATH = "images"   
			+ System.getProperty("file.separator");   

	private static final String[] FILTER_NAMES = 
		{ "XML Files (*.xml)", "All Files (*.*)" };   

	private static final String[] FILTER_EXTS = { "*.xml", "*.*"};   

	//------------------------------------------------------------------
	// the pointer to the app
	private static final MOOSyCA_GUI _theApp = new MOOSyCA_GUI();
	/**  
	 * Gets the running application  
	 *   
	 * @return MOOSyCA_GUI  
	 */   
	public static MOOSyCA_GUI getApp() { return _theApp; }   

	//------------------------------------------------------------------
	// JFaca events/action stuff
	private MyStatusAction _statusAction;
	private ActionContributionItem _aciStatusAction;
	
	private MyOptionsAction _optionsAction;
	private ActionContributionItem _aciOptionsAction;

	private MyMatlabConnectAction _matlabConnectAction;
	private ActionContributionItem _aciMatlabConnectAction;

	private MyHelpAction _helpAction;
	private ActionContributionItem _aciHelpAction;
	
	private MyToggleProjectViewAction _toggleProjectViewAction;
	private ActionContributionItem _aciToggleProjectViewAction;

	private MyToggleMessageViewAction _toggleMessageViewAction;
	private ActionContributionItem _aciToggleMessageViewAction;

	private MyTestMatlabAction _testMatlabAction;
	private ActionContributionItem _aciTestMatlabAction;

	private MyXMLViewAction _xmlViewAction;
	private ActionContributionItem _aciXMLViewAction;
	
	private Action _exitAction;
		
	//------------------------------------------------------------------
	private MyTopLevelComposite _topLevelContainer = null;
	/**  
	 * Gets the application top-level composite  
	 *   
	 * @return MyTopLevelComposite  
	 */   
	public MyTopLevelComposite getTopLevelComposite() { return _topLevelContainer; }   
	
	//------------------------------------------------------------------
	private MenuManager _mainMenuManager = null;
	/**  
	 * Gets the application menu bar  
	 *   
	 * @return MenuManager  
	 */   
	public MenuManager getMainMenuManager() { return _mainMenuManager; }   

	//------------------------------------------------------------------
	private ToolBarManager _toolBarManager = null;
	/**  
	 * Gets the application toolbar 
	 *   
	 * @return ToolBarManager  
	 */   
	public ToolBarManager getToolBarManager() { return _toolBarManager; }   
	
	//------------------------------------------------------------------
	private StatusLineManager _statusLineManager = new StatusLineManager();
	/**  
	 * Gets the application status line 
	 *   
	 * @return StatusLineManager  
	 */   
	public StatusLineManager getStatusLineManager() { return _statusLineManager; }   
	
	//------------------------------------------------------------------
	private MyMatlabControlManager _matlabControlManager = null;
	/**  
	 * Gets the application Matlab control manager 
	 *   
	 * @return MyMatlabControlManager  
	 */   
	public MyMatlabControlManager getMatlabControlManager() { return _matlabControlManager; }
		
	//------------------------------------------------------------------
	/**
	 * Create the application window.
	 */
	public MOOSyCA_GUI() {
		super(null);
		
		//------------------------------------------------------------------
		// matlabcontrol stuff
		_matlabControlManager = new MyMatlabControlManager();
		System.setSecurityManager(_matlabControlManager.getPermissiveSecurityManager());

		//------------------------------------------------------------------
		// GUI stuff
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		
		_topLevelContainer = new MyTopLevelComposite(parent, SWT.NONE);

		// pass pointers as needed
		_matlabConnectAction.setTopLevelContainer(_topLevelContainer);
				
		// place the ActionContributionItem in the GUI
		_aciStatusAction.fill(parent);
		_aciToggleProjectViewAction.fill(parent);
		_aciToggleMessageViewAction.fill(parent);
		_aciOptionsAction.fill(parent);
		_aciMatlabConnectAction.fill(parent);
		_aciHelpAction.fill(parent);
		_aciXMLViewAction.fill(parent);

		return _topLevelContainer;
	}
	
	/**
	 * Create the actions.
	 */
	private void createActions() {

		// Test the status line
		// TODO: remove soner or later
		_statusAction = new MyStatusAction(_statusLineManager);
		_aciStatusAction = new ActionContributionItem(_statusAction);
		
		// Test the options
		// TODO: open a preference JFace Dialog
		_optionsAction = new MyOptionsAction(_statusLineManager);
		_aciOptionsAction = new ActionContributionItem(_optionsAction);

		// Left-hand-sid project panel view
		// TODO: improve appearance, check tree viewers
		_toggleProjectViewAction = new MyToggleProjectViewAction(_statusLineManager);
		_aciToggleProjectViewAction = new ActionContributionItem(_toggleProjectViewAction);

		// Message window at the bottom of the GUI
		// TODO: allow font settings
		_toggleMessageViewAction = new MyToggleMessageViewAction(_statusLineManager);
		_aciToggleMessageViewAction = new ActionContributionItem(_toggleMessageViewAction);

		// Connection to Matlab via a dialog
		// TODO: use a JFace Dialog instead of a SWT Dialog
		_matlabConnectAction = new MyMatlabConnectAction( _statusLineManager );
		_matlabConnectAction.setMatlabControlManager(_matlabControlManager); // pass pointers as needed
		_aciMatlabConnectAction = new ActionContributionItem(_matlabConnectAction);

		// Inspect XML files via a dialog
		// TODO: use a JFace Dialog instead of a SWT Dialog
		// TODO: improve MyXMLDocument to inspect files as needed 
		_xmlViewAction = new MyXMLViewAction(_statusLineManager);
		_aciXMLViewAction = new ActionContributionItem(_xmlViewAction);

		// Matlab command test dialog
		_testMatlabAction = new MyTestMatlabAction(_statusLineManager);
		_aciTestMatlabAction = new ActionContributionItem(_testMatlabAction);
		
		// Help dialog
		_helpAction = new MyHelpAction(_statusLineManager);
		_aciHelpAction = new ActionContributionItem(_helpAction);
		
		// Exit action
		_exitAction = new Action("&Exit@Ctrl+Z") { 			
			@Override 			
			public void run() { 				
				close(); 			
			} 		
		};
		
	}

	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		//MenuManager menuManager = new MenuManager("menu");
		//return menuManager;

		_mainMenuManager = new MenuManager(null);
		
		MenuManager fileMenu = new MenuManager("&File");
		fileMenu.add(_statusAction);
		fileMenu.add(new Separator());
		fileMenu.add(_exitAction);
		
		MenuManager viewMenu = new MenuManager("&View");
		viewMenu.add(_toggleProjectViewAction);
		viewMenu.add(_toggleMessageViewAction);
		viewMenu.add(new Separator());
		viewMenu.add(_xmlViewAction);
		
		MenuManager matlabMenu = new MenuManager("&Matlab");
		matlabMenu.add(_optionsAction);
		matlabMenu.add(_matlabConnectAction);
		matlabMenu.add(new Separator());
		matlabMenu.add(_testMatlabAction);

		MenuManager helpMenu = new MenuManager("&Help");
		helpMenu.add(_helpAction);

		_mainMenuManager.add(fileMenu);
		_mainMenuManager.add(viewMenu);
		_mainMenuManager.add(matlabMenu);
		_mainMenuManager.add(helpMenu);

		return _mainMenuManager;
	}

	/**
	 * Create the toolbar manager.
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		_toolBarManager = new ToolBarManager(style);
		_toolBarManager.add(_statusAction);
		_toolBarManager.add(_xmlViewAction);
		_toolBarManager.add(_toggleProjectViewAction);
		_toolBarManager.add(_toggleMessageViewAction);
		_toolBarManager.add(_optionsAction);
		_toolBarManager.add(_matlabConnectAction);
		_toolBarManager.add(_testMatlabAction);
		_toolBarManager.add(_helpAction);
		return _toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * @return the StatusLineManager
	 */
	@Override
	public StatusLineManager createStatusLineManager() {
		//_statusLineManager = new StatusLineManager();
		return _statusLineManager;
	}

	public MyMatlabControlManager getMyMatlabControlManager()
	{
		return _matlabControlManager;
	}
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[])
			throws MatlabConnectionException, MatlabInvocationException
	{
		try {
			_theApp.setBlockOnOpen(true);
			_theApp.open();			
			Display.getCurrent().dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("New Application");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		
		Rectangle monitorArea = getShell().getDisplay().getPrimaryMonitor().getBounds();
		int width = (int)((double)monitorArea.width * 3.0/4.0);
		int height = (int)((double)monitorArea.height * 4.0/5.0);	
		return new Point(width, height);
	}

}// end of class definition