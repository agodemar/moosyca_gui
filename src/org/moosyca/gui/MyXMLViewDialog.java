package org.moosyca.gui;

import java.io.IOException;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.*;   
import org.eclipse.swt.layout.*;   
import org.eclipse.swt.widgets.*;   

public class MyXMLViewDialog extends Dialog 
{
	
	/** The path to the directory containing the images */   
	public static final String IMAGE_PATH = "images"   
			+ System.getProperty("file.separator");   

	private static final String[] FILTER_NAMES = 
		{ "XML Files (*.xml)", "All Files (*.*)" };   

	private static final String[] FILTER_EXTS = { "*.xml", "*.*"};   
	
	private Shell _parentShell;
	private TabFolder _tabFolder; 
	private MyXMLViewToolbar _toolbar;
	
	private static MyXMLViewDialog _theDialog;
	public static MyXMLViewDialog getDialog(){ return _theDialog; }
	
	//	public MyMatlabConnectDialog(Shell parentShell) 
	//	{
	//		super(parentShell);
	//	}

	public MyXMLViewDialog(
			Shell parentShell, 
			MyTopLevelComposite tlComposite
			) 
	{
		super(parentShell);
		
		_parentShell = parentShell;
		_theDialog = this;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#setShellStyle(int)
	 */
	protected void setShellStyle(int arg0) 
	{
		//Use the following not to show the default close X button in the title bar
		super.setShellStyle(
				SWT.TITLE |
				SWT.RESIZE |
				SWT.CLOSE
				);
		
	}	

	@Override
	   protected void createButtonsForButtonBar(Composite parent) {
		// nothing here if OK and CANCEL buttons not desired
//	    super.createButtonsForButtonBar(parent);
//
//	    Button ok = getButton(IDialogConstants.OK_ID);
//	    ok.setText("OOkk");
//	    setButtonLayoutData(ok);
//
//	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
//	    cancel.setText("CCCancel");
//	    setButtonLayoutData(cancel);
	 }
	
	@Override
	protected Control createDialogArea(Composite parent) {
		/*
		 * Create the dialog area where you can place the UI components
		 */
		// A dialog box with no buttons at all press 'ESC' to close
		Composite composite = ( Composite )super.createDialogArea(parent);
		//Set the shell message
		composite.getShell().setText("Explore XML file");
		try 
		{
			FormLayout layout = new FormLayout();
			layout.marginHeight = 12;
			layout.marginWidth = 12;
			composite.setLayout(layout);

			// Create the menu   
			composite.getShell().setMenuBar(
					new MyXMLViewMenu(composite.getShell()).getMenu()
					);   

			// Create the toolbar and attach it to the top of the window   
			_toolbar = new MyXMLViewToolbar(composite);
			FormData data1 = new FormData();   
			data1.top = new FormAttachment(0, 0);   
			data1.left = new FormAttachment(0, 0);   
			data1.right = new FormAttachment(100, 0);   
			_toolbar.getToolBar().setLayoutData(data1);  
			
			// Create the containing tab folder   
			_tabFolder = new TabFolder(composite, SWT.TOP);   
			FormData data2 = new FormData();   
			data2.top = new FormAttachment(_toolbar.getToolBar(), 0);   
			data2.bottom = new FormAttachment(100, 0);   
			data2.left = new FormAttachment(0, 0);   
			data2.right = new FormAttachment(100, 0);   
			_tabFolder.setLayoutData(data2);  

		}
		catch (Exception e) 
		{
			showError(e.getMessage());   
		}
		
		composite.getShell().pack();
		return composite;
	}
	

	/**  
	 * Opens a file  
	 */   
	public void openFile() {   
		// Show the file open dialog   
		FileDialog dlg = new FileDialog(_parentShell);   
		dlg.setFilterNames(FILTER_NAMES);   
		dlg.setFilterExtensions(FILTER_EXTS);   
		String fileName = dlg.open();   
		if (fileName != null) {   
			//-----------------------------------------------
			// A file was selected, so load it   
			MyXMLDocument doc = new MyXMLDocument(fileName);   
			try {   
				// Open the file   
				doc.open();   

				// Create a tab to hold the file   
				TabItem item = new TabItem(_tabFolder, SWT.NONE);   

				// Create the contents for the tab, set it into the tab,   
				// and set its text to the filename   
				MyXMLViewTab tab = new MyXMLViewTab(_tabFolder, doc);   
				item.setControl(tab);   
				item.setText(tab.getText());   

				// Select the tab   
				_tabFolder.setSelection(new TabItem[] { item});
				
			} catch (IOException e) {   
				showError(e.getMessage());   
			}   
		}   
	}   

	/**  
	 * Closes a file  
	 */   
	public void closeFile() {   
		TabItem[] items = _tabFolder.getSelection();   
		for (int i = 0, n = items.length; i < n; i++) {   
			items[i].dispose();   
		}   
	}   

	/**  
	 * Shows the about box  
	 */   
	public void about() {   
		MessageBox mb = new MessageBox(_parentShell, SWT.ICON_INFORMATION | SWT.OK);   
		mb.setText("About");   
		mb.setMessage("XML View\nAn XML file viewer written in SWT"   
				+ "\nUses JDOM (http://www.jdom.org/)");   
		mb.open();   
	}   	

	  /**  
	   * Shows an error  
	   *   
	   * @param s the error to show  
	   */   
	  private void showError(String s) {   
	    MessageBox mb = new MessageBox(_parentShell, SWT.ICON_ERROR | SWT.OK);   
	    mb.setText("Error");   
	    mb.setMessage(s);   
	    mb.open();   
	  }   

}
