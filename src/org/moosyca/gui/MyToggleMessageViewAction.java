package org.moosyca.gui;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;

public class MyToggleMessageViewAction extends Action
{
	public MyToggleMessageViewAction(StatusLineManager sm)
	{
		super("&Toggle log message view@Ctrl+M", AS_PUSH_BUTTON);
		
		setToolTipText("Toggle log message window view");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/TextPlain_32x32.png")
				);
	}
	public void run()
	{
		// get sash form weights and resize accordingly
		int [] weights = MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormUpDw().getWeights();
		if ( weights[1] == 0 )
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormUpDw().setWeights(new int[] { 4, 1});			
		}
		else
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormUpDw().setWeights(new int[] { 4, 0});			
		}
		
	}// end of run method

}