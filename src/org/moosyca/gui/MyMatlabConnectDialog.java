package org.moosyca.gui;

import java.util.concurrent.atomic.AtomicReference;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;

public class MyMatlabConnectDialog extends Dialog 
{

	//Status messages
	private static final String STATUS_DISCONNECTED = "Status: Disconnected",
			STATUS_CONNECTING = "Status: Connecting",
			STATUS_CONNECTED_EXISTING = "Status: Connected (Existing)",
			STATUS_CONNECTED_LAUNCHED = "Status: Connected (Launched)";

	//Return messages
	private static final String RETURNED_DEFAULT = "Returned Object / Java Exception",
			RETURNED_OBJECT = "Returned Object",
			RETURNED_EXCEPTION = "Java Exception";	

	private MyTopLevelComposite _topLevelContainer = null;
	private MyMatlabControlManager _matlabControlManager = null;

	private Label _mainLabel = null;
	private ProgressBar _progressBar = null;
	private Button _buttonConnect = null;
	private Button _buttonDisconnect = null;

	//	public MyMatlabConnectDialog(Shell parentShell) 
	//	{
	//		super(parentShell);
	//	}

	public MyMatlabConnectDialog(Shell parentShell) 
	{
		super(parentShell);
		_topLevelContainer = MOOSyCA_GUI.getApp().getTopLevelComposite();
		_matlabControlManager = MOOSyCA_GUI.getApp().getMatlabControlManager();

		//		// LOG MESSAGE
		//		_topLevelContainer.getMessageTextWindow().append(
		//				"COSTRUCTOR:: connected: " + 
		//						_topLevelContainer.isConnectedToMatlab() + "\n"
		//				);

	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#setShellStyle(int)
	 */
	protected void setShellStyle(int arg0) 
	{
		//Use the following not to show the default close X button in the title bar
		super.setShellStyle(SWT.TITLE);
	}	

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// nothing here if OK and CANCEL buttons not desired
		//	    super.createButtonsForButtonBar(parent);
		//
		//	    Button ok = getButton(IDialogConstants.OK_ID);
		//	    ok.setText("OOkk");
		//	    setButtonLayoutData(ok);
		//
		//	    Button cancel = getButton(IDialogConstants.CANCEL_ID);
		//	    cancel.setText("CCCancel");
		//	    setButtonLayoutData(cancel);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		/*
		 * Create the dialog area where you can place the UI components
		 */
		// A dialog box with no buttons at all press 'ESC' to close
		Composite composite = ( Composite )super.createDialogArea(parent);
		//Set the shell message
		composite.getShell().setText("Matlab connection");
		try 
		{
			FormLayout layout = new FormLayout();
			layout.marginHeight = 12;
			layout.marginWidth = 12;
			composite.setLayout(layout);
			
			// Place all your UI Components
			{
				//-----------------------------------------------------------------
				// The Label
				_mainLabel = new Label(composite, SWT.None );
				//if ( !_topLevelContainer.isConnectedToMatlab() ) 
				if ( !_matlabControlManager.isConnected() ) 
					_mainLabel.setText(STATUS_DISCONNECTED);
				else 
					_mainLabel.setText(STATUS_CONNECTED_EXISTING);
				FormData lblData = new FormData();
				lblData.top =  
						new FormAttachment(0, 0); // y, offset
				lblData.left =  
						new FormAttachment(0, 0);// x, offset
				_mainLabel.setLayoutData(lblData);

				//-----------------------------------------------------------------
				// The progress bar
				_progressBar = new ProgressBar(composite, SWT.SMOOTH);
				_progressBar.setMinimum(0);
				_progressBar.setMaximum(100);
				if ( _matlabControlManager.isConnected() ) _progressBar.setSelection(100);
				else _progressBar.setSelection(0);

				FormData progressBarData = new FormData();
				progressBarData.top = new FormAttachment(_mainLabel,10);					
				_progressBar.setLayoutData(progressBarData);

				//-----------------------------------------------------------------
				// The connect button
				_buttonConnect = new Button(composite, SWT.PUSH);
				_buttonConnect.setText("Connect");
				_buttonConnect.setEnabled(
						!_matlabControlManager.isConnected() //  _topLevelContainer.isConnectedToMatlab()
						);
				FormData buttonConnectData = new FormData();
				buttonConnectData.top = new FormAttachment(_progressBar,10);				
				_buttonConnect.setLayoutData(buttonConnectData);

				// Write listener for Connect button
				_buttonConnect.addSelectionListener( 
						new SelectionAdapter()
						{
							public void widgetSelected(SelectionEvent se) 
							{
								_topLevelContainer.getMessageTextWindow().append(
										"Connection to Matlab attempted\n"
										);

								//							// LOG MESSAGE
								//							_topLevelContainer.getMessageTextWindow().append(
								//									"CONNECT BUTTON:: connected: " + 
								//											_topLevelContainer.isConnectedToMatlab() + "\n"
								//									);

								//-----------------------------------------------------------------
								// Attempt the connection
								doConnect();
							}
						}
						);

				//-----------------------------------------------------------------
				// The disconnect button
				_buttonDisconnect = new Button(composite, SWT.PUSH);
				_buttonDisconnect.setText("Disconnect");
				_buttonDisconnect.setEnabled(
						_matlabControlManager.isConnected() // _topLevelContainer.isConnectedToMatlab()
						);
				FormData buttonDisconnectData = new FormData();
				buttonDisconnectData.left = new FormAttachment(_buttonConnect,10);
				buttonDisconnectData.top = new FormAttachment(_buttonConnect,0,SWT.CENTER);
				_buttonDisconnect.setLayoutData(buttonDisconnectData);

				// Write listener for Disconnect button
				_buttonDisconnect.addSelectionListener( 
						new SelectionAdapter()
						{
							public void widgetSelected(SelectionEvent se) 
							{
								_topLevelContainer.getMessageTextWindow().append(
										"Disconnection from Matlab attempted\n"
										);

								//							// LOG MESSAGE
								//							_topLevelContainer.getMessageTextWindow().append(
								//									"DISCONNECT BUTTON:: connected: " + 
								//											_topLevelContainer.isConnectedToMatlab() + "\n"
								//									);

								//-----------------------------------------------------------------
								// Attempt the disconnection
								boolean disconnectionStatus = doDisconnect();
								if ( disconnectionStatus ) toggleConnectionStatus();

							}
						}
						);

				//-----------------------------------------------------------------
				// The done button
				Button buttonDone = new Button(composite, SWT.PUSH);
				buttonDone.setText("Done");
				buttonDone.setEnabled(true);
				FormData buttonDoneData = new FormData();
				buttonDoneData.left = new FormAttachment(_buttonDisconnect,10);
				buttonDoneData.top = new FormAttachment(_buttonDisconnect,0,SWT.CENTER);
				buttonDone.setLayoutData(buttonDoneData);

				//Write listener for Done button
				buttonDone.addSelectionListener( 
						new SelectionAdapter()
						{
							public void widgetSelected(SelectionEvent se) 
							{
								_topLevelContainer.getMessageTextWindow().append(
										"Matlab connection dialog closed\n"
										);

								// pass the ultimate connection status info to toplevel container
								_topLevelContainer.setConnectedToMatlab(
										//!_buttonConnect.getEnabled()
										_matlabControlManager.isConnected()
										);
								close();
							}
						}
						);

			}// end-of-scope
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		// pack dialog's contents
		composite.getShell().pack();
		// Set the dialog position in the middle of the monitor
		setDialogLocation();
		
		return composite;
	}// end of constructor

	/*
	// overriding this methods allows you to set the
	// title of the custom dialog
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Matlab connect dialog");
	}
	 */

	/*
	@Override
	protected Point getInitialSize() {
		return new Point(450, 300);
	}
	 */

	//~~ Utility methods

	/**
	 * Method used to set the dialog in the centre of the monitor
	 */
	private void setDialogLocation()
	{
		Rectangle monitorArea = getShell().getDisplay().getPrimaryMonitor().getBounds();
		Rectangle shellArea = getShell().getBounds();
		int x = monitorArea.x + (monitorArea.width - shellArea.width)/2;
		int y = monitorArea.y + (monitorArea.height - shellArea.height)/2;
		getShell().setLocation(x,y);
	}		

	/**
	 * Method used to revert the "Enabled/Disabled" status
	 * of Connect and Disconnect buttons
	 */
	private void toggleConnectionStatus()
	{
		//_matlabControlManager.toggleConnectionStatus();
		// pass the info about Matlab connection status to the toplevel composite
		_topLevelContainer.toggleConnectionStatus();

		//		// LOG MESSAGE
		//		_topLevelContainer.getMessageTextWindow().append(
		//				"TOGGLE FUNC:: connected: " + 
		//						_topLevelContainer.isConnectedToMatlab() + "\n"
		//				);

		_buttonDisconnect.setEnabled(
				!_buttonDisconnect.getEnabled()
				);
		_buttonConnect.setEnabled(
				!_buttonConnect.getEnabled()
				);
		if ( _buttonConnect.getEnabled() )
		{
			_progressBar.setSelection(0);			
		}
		else
		{
			_progressBar.setSelection(100);			
		}

	}

	private void doConnect()
	{
		// Proxy to communicate with MATLAB
		final MatlabProxyFactory factory = _matlabControlManager.getProxyFactory();
		final AtomicReference<MatlabProxy> proxyHolder = _matlabControlManager.getProxyHolder(); 

		// Connect to MATLAB
		try {

			// Request a proxy
			factory.requestProxy(new MatlabProxyFactory.RequestCallback()
			{
				@Override
				public void proxyCreated(final MatlabProxy proxy)
				{
					//proxyHolder.set(proxy);
					_matlabControlManager.setProxy(proxy);

					proxy.addDisconnectionListener(new MatlabProxy.DisconnectionListener()
					{
						@Override
						public void proxyDisconnected(MatlabProxy proxy)
						{
							//proxyHolder.set(null);
							_matlabControlManager.setProxy(null);

							//Visual update
							Display.getDefault().asyncExec(new Runnable() {
								@Override
								public void run()
								{
									_buttonConnect.setEnabled(true);
									_buttonDisconnect.setEnabled(false);
									_progressBar.setSelection(0);
									_mainLabel.setText(STATUS_DISCONNECTED);
								}
							});
						}
					});

					//Visual update
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run()
						{
							String strStatus;
							if( _matlabControlManager.getProxy().isExistingSession() )
							{
								strStatus = STATUS_CONNECTED_EXISTING;
							}
							else
							{
								strStatus = STATUS_CONNECTED_LAUNCHED;
							}
							_mainLabel.setText(strStatus);
							_buttonConnect.setEnabled(false);
							_buttonDisconnect.setEnabled(true);
							_progressBar.setSelection(100);
						}
					});

					// Matlab Control Manager update
					//_matlabControlManager.setConnected( proxyHolder.get().isConnected() );

					// try to fire a command
					// TODO: move it around in a proper place
					try {
						_matlabControlManager.getProxy().eval("disp('hello world')");
					} catch (MatlabInvocationException e) {
						// e.printStackTrace();
						showError( 
								"Matlab invocation error: " +
										e.getMessage() 
								);
					}
				}
			});
		}
		catch (MatlabConnectionException e)
		{
			showError( 
					"Matlab connection error: " +
							e.getMessage() 
					);
		}

	}// end of doConnect

	private boolean doDisconnect()
	{
		_matlabControlManager.getProxy().disconnect();
		return !_matlabControlManager.getProxy().isConnected();
	}

	/**  
	 * Shows an error  
	 *   
	 * @param s the error to show  
	 */   
	private void showError(String s) {   
		MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR | SWT.OK);   
		mb.setText("Error");
		mb.setMessage(s);   
		mb.open();   
	}   


}// end of class definition
