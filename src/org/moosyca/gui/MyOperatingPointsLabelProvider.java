package org.moosyca.gui;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class MyOperatingPointsLabelProvider extends LabelProvider 
{

	private static final Image FOLDER_IMG = getImage("Folder_32x32.png");
	private static final Image FOLDER_AIRCRAFT_IMG = getImage("FolderAirplane_32x32.png");
	private static final Image AEROCOMPONENT_IMG = getImage("AirplanePaper_32x32.png");
	private static final Image ECLIPSE_IMG = getImage("eclipse32.png");

	@Override
	public String getText(Object element) {
		if (element instanceof MyOperatingPoint) {
			MyOperatingPoint op = (MyOperatingPoint) element;
			return op.getName();
		}
		return "null";
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof MyOperatingPoints) {
			return FOLDER_IMG;
		}
		if (element instanceof MyOperatingPoint) {
			return FOLDER_AIRCRAFT_IMG;
		}
		return ECLIPSE_IMG;
	}
	
	
	// Helper Method to load the images
	private static Image getImage(String file) {
		ImageDescriptor image = 
				ImageDescriptor.createFromFile(
						MOOSyCA_GUI.class,
						"images/" + file
						);
		return image.createImage();
	} 

}
