package org.moosyca.gui;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;
import org.eclipse.swt.widgets.Display;

public class MyToggleProjectViewAction extends Action
{
	public MyToggleProjectViewAction(StatusLineManager sm)
	{
		super("&Toggle project view@Ctrl+P", AS_PUSH_BUTTON);
		
		setToolTipText("Toggle project pannel view on left side");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/TreeView_32x32.png")
				);
	}
	public void run()
	{
		// get sash form weights and resize accordingly
		int [] weights = MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().getWeights();
		if ( weights[0] == 0 )
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().setWeights(new int[] { 1, 4});
		}
		else
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getSashFormLeftRight().setWeights(new int[] { 0, 4});			
		}
		
	}// end of run method

}
