package org.moosyca.gui;

import java.util.ArrayList;
import java.util.List;

public class MyWorkSession {

	private String _name = null;
	private MyAircraft _aircraft = null;
	private List<MyOperatingPoint> _operatingPoints = new ArrayList<MyOperatingPoint>();
	private List<MyAnalysis> _analyses = new ArrayList<MyAnalysis>();
	
	public MyWorkSession() {
	}
	public MyWorkSession(MyAircraft ac) {
		this._aircraft = ac;
	}
	public MyWorkSession(MyAircraft ac, MyOperatingPoint op) {
		this._aircraft = ac;
		this._operatingPoints.add(op);
	}

	public void initSession(MyAircraft ac) {
		_aircraft = ac;
	}
	public void initSession(MyAircraft ac, MyOperatingPoint op) {
		_aircraft = ac;
		_operatingPoints.add(op);
	}
	
	public void addOperatingPoint(MyOperatingPoint op)
	{
		_operatingPoints.add(op);
	}
	public void removeOperatingPoint(MyOperatingPoint op)
	{
		_operatingPoints.remove(op);
	}
	public void resetOperatingPoint(MyOperatingPoint op)
	{
		_operatingPoints.clear();
	}
	public List<MyOperatingPoint> getOperatingPoints(){
		return _operatingPoints;
	}	
	public int getNOperatingPoints(){
		return _operatingPoints.size();
	}
	
	public void addAnalysis(MyAnalysis a)
	{
		_analyses.add(a);
	}
	public void removeAnalysis(MyAnalysis a)
	{
		_analyses.remove(a);
	}
	public List<MyAnalysis> getAnalises(){
		return _analyses;
	}	
	public int getNAnalysess(){
		return _analyses.size();
	}

	public void setName(String name) {
		this._name = name;
	}
	public String getName() {
		return this._name;
	}

	public void setAircraft(MyAircraft ac) {
		this._aircraft = ac;
	}
	public MyAircraft getAircraft() {
		return this._aircraft;
	}
		

}
