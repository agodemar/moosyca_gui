package org.moosyca.gui;

import java.text.MessageFormat;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

public class MyHelpAction extends Action
{
	StatusLineManager statman;
	short triggercount = 0;
	
	public MyHelpAction(StatusLineManager sm)
	{
		super("&About@Ctrl+H", AS_PUSH_BUTTON);
		
		statman = sm;
		setToolTipText("Get some help");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/Help_32x32.png")
				);

	}
	public void run()
	{
		triggercount++;
		statman.setMessage(
				"The help action has fired. Count: " +
				triggercount
				);
		
		// display a modal message box
		MessageBox box = new MessageBox(				
				Display.getCurrent().getActiveShell(), 
				SWT.ICON_INFORMATION | SWT.OK);
		box.setText("About");
		String msg = MessageFormat.format(
				"MOOSyCA\nAgodemar\nRunning on {0}",
				new Object[] { System.getProperty("os.name") }
				);
		box.setMessage(msg);
		box.open();

	}
}