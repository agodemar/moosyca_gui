package org.moosyca.gui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;

public class MyTestMatlabDialog extends InputDialog
{

	protected MyTestMatlabDialog(
			Shell parentShell, 
			String error1errorMessage1,
			String error1errorMessage2,
			String error1errorMessage3,
			IInputValidator validator
			) 
	{
		super(
				parentShell, 
				"Error, Test Matlab command", 
				"Error, Test Matlab command", 
				"Error, Test Matlab command", 
				null // validator
				);
	}

}
