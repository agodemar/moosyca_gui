package org.moosyca.gui;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.draw2d.LightweightSystem;
import org.eclipse.nebula.visualization.xygraph.dataprovider.CircularBufferDataProvider;
import org.eclipse.nebula.visualization.xygraph.figures.Trace;
import org.eclipse.nebula.visualization.xygraph.figures.Trace.PointStyle;
import org.eclipse.nebula.visualization.xygraph.figures.XYGraph;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureCanvas;


public class MyTabbedPane extends Composite
{
	private MyJOGLContainer _joglContainer = null;
	public MyJOGLContainer getJOGLContainer() { return _joglContainer; }
	
	public MyTabbedPane(Composite parent, int style) {

		super(parent, style);

		FillLayout compositeLayout = new FillLayout(); 
		setLayout(compositeLayout);

		CTabFolder tabFolder = new CTabFolder(this, SWT.NONE);

		//--------------------------------------------------------------------------------
		// Tab with 3D view
		CTabItem tabItem3DView = new CTabItem(tabFolder, SWT.NULL);
		tabItem3DView.setText("3D View");


		// Put some plots on first tab
		FigureCanvas fc1 = new FigureCanvas(tabFolder); 
		fc1.setContents(new Figure());
		fc1.setScrollBarVisibility(FigureCanvas.NEVER);

		tabItem3DView.setControl(fc1);

		//use LightweightSystem to create the bridge between SWT and draw2D
		//final LightweightSystem lws = new LightweightSystem(shell);
		final LightweightSystem lws = new LightweightSystem();

		//create a new XY Graph.
		XYGraph xyGraph = new XYGraph();
		xyGraph.setTitle("Simple Example");
		//set it as the content of LightwightSystem
		lws.setContents(xyGraph);

		//create a trace data provider, which will provide the data to the trace.
		CircularBufferDataProvider traceDataProvider = new CircularBufferDataProvider(false);
		traceDataProvider.setBufferSize(100);		
		traceDataProvider.setCurrentXDataArray(new double[]{10, 23, 34, 45, 56, 78, 88, 99});
		traceDataProvider.setCurrentYDataArray(new double[]{11, 44, 55, 45, 88, 98, 52, 23});	

		//create the trace
		Trace trace = new Trace("Trace1-XY Plot", 
				xyGraph.primaryXAxis, xyGraph.primaryYAxis, traceDataProvider);			

		//set trace property
		trace.setPointStyle(PointStyle.XCROSS);

		//add the trace to xyGraph
		xyGraph.addTrace(trace);			

		lws.setControl(fc1);



		//--------------------------------------------------------------------------------
		// Dummy tab

		CTabItem tabItem1 = new CTabItem(tabFolder, SWT.CLOSE);
		tabItem1.setText("Tab 1");
		Text text = 
				new Text(tabFolder, 
						// SWT.NULL
						SWT.MULTI
						| SWT.BORDER
						| SWT.H_SCROLL
						| SWT.V_SCROLL
						);
		text.setText("This is page 1");
		tabItem1.setControl(text);

		//--------------------------------------------------------------------------------
		// Dummy tab

		CTabItem tabItem2 = new CTabItem(tabFolder, SWT.CLOSE);
		//tabItem2.setText("Test LWJGL");
		//MyOpenGLContainer oglContainer = new MyOpenGLContainer(tabFolder,SWT.NONE);
		
		tabItem2.setText("Test JOGL");
		_joglContainer = new MyJOGLContainer(tabFolder,SWT.NONE);
		tabItem2.setControl(_joglContainer);
		
		// preferred size
//		tabFolder.pack();
//		pack();
				
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}