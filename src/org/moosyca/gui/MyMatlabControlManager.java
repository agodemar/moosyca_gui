package org.moosyca.gui;

import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import matlabcontrol.MatlabInvocationException;
import matlabcontrol.PermissiveSecurityManager;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;

public class MyMatlabControlManager {

	PermissiveSecurityManager _permissiveSecurityManager = null;
	MatlabProxyFactoryOptions _proxyFactoryOptions = null;
	
    // Factory to create proxy
    private final MatlabProxyFactory _factory;
    
    // Proxy to communicate with MATLAB
    private final AtomicReference<MatlabProxy> _proxyHolder = new AtomicReference<MatlabProxy>();
	
    // store the connection status
	private static boolean _isConnected = false;

	// store the command to be sent to Matlab
	private String _testMatlabCommand = null;

	// constructor
	public MyMatlabControlManager()
	{
		_permissiveSecurityManager = new PermissiveSecurityManager();
		
		_proxyFactoryOptions =
				new MatlabProxyFactoryOptions.Builder()
				    .setUsePreviouslyControlledSession(true)
				    //.setHidden(true)
				    .setMatlabLocation(null)
				    .build(); 
		
		_factory = new MatlabProxyFactory(_proxyFactoryOptions);
		
		// init connection status
		if (_proxyHolder.get() != null)
			_isConnected = _proxyHolder.get().isConnected();
		else
			_isConnected = false;
		
	}
	
	public PermissiveSecurityManager getPermissiveSecurityManager()
	{
		return _permissiveSecurityManager;
	}
	
	MatlabProxyFactory getProxyFactory()
	{
		return _factory;
	}
	public MatlabProxyFactoryOptions getMatlabProxyFactoryOptions()
	{
		return _proxyFactoryOptions;
	}
	public void setMatlabProxyFactoryOptions(MatlabProxyFactoryOptions options)
	{
		_proxyFactoryOptions = options;
	}
	public AtomicReference<MatlabProxy> getProxyHolder()
	{
		return _proxyHolder;
	}

	public void setProxy(MatlabProxy proxy) { _proxyHolder.set(proxy); }
	public MatlabProxy getProxy() { return _proxyHolder.get(); }

	public boolean isConnected() 
	{
		if (_proxyHolder.get() != null)
			_isConnected = _proxyHolder.get().isConnected();
		else
			_isConnected = false;
			
		return _isConnected; 
	}
	public boolean disconnect() { 
		_proxyHolder.get().disconnect();
		_isConnected = _proxyHolder.get().isConnected();
		return !_isConnected;
	}

	public void setTestMatlabCommand(String command) { _testMatlabCommand = command; }
	
	public void evaluateMatlabCommand(String command) {
		if (_proxyHolder.get() != null)
		{
			try {
				// fire the command to Matlab
				_proxyHolder.get().eval( command );
				
			} catch (MatlabInvocationException e) {
				// e.printStackTrace();
				showError( 
						"Matlab invocation error: " +
								e.getMessage() 
						);
			}
		}
	}

	/**  
	 * Shows an error  
	 *   
	 * @param s the error to show  
	 */   
	private void showError(String s) {   
		MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_ERROR | SWT.OK);   
		mb.setText("Error");
		mb.setMessage(s);   
		mb.open();   
	}   

}// end-of-class definition
