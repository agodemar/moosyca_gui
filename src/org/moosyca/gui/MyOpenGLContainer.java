package org.moosyca.gui;

import java.text.MessageFormat;

import org.eclipse.swt.*;
import org.eclipse.swt.layout.*;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.opengl.GLCanvas;
import org.eclipse.swt.opengl.GLData;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;
import org.lwjgl.LWJGLException;
import org.eclipse.swt.widgets.Display;
import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.DisplayMode;

public class MyOpenGLContainer extends Composite
{

	private final GLCanvas _glCanvas;
	public GLCanvas getGLCanvas() { return _glCanvas; }
	
	Point[] _ptUpperLeft = null;

	Point[] _ptMouseDown = null;
	Point[] _ptMouseUp = null;
	Point[] _ptCurrent = null;
	Point[] _ptPrevious = null;
	boolean _mouseGrabbed = false;
	float _dX, _dY;
	final int[] _lastPressedButton = new int[1];
	
	private static int _rotation = 0;
	public static void setRotation(int rot){ _rotation = rot; };
	public static int getRotation(){ return _rotation; }
	
	float _bgRed = .3f; 
	float _bgGreen = .5f; 
	float _bgBlue = .8f;
	float _bgAlpha = 1.0f;
	
	private final MyLWJGLEulerCamera _camera =
			new MyLWJGLEulerCamera.Builder().setAspectRatio(640f / 480f).setFieldOfView(60).build();
	public MyLWJGLEulerCamera getCamera() { return _camera; }
	
	public MyOpenGLContainer(Composite parent, int style) 
	{			
		super(parent, style);
		
		this.setLayout(new FillLayout());
		GLData data = new GLData ();
		data.doubleBuffer = true;
		// final GLCanvas 
		_glCanvas = new GLCanvas(this, SWT.NONE, data);
		_glCanvas.setCurrent();
		try {
			GLContext.useContext(_glCanvas);
		} catch(LWJGLException e) { e.printStackTrace(); }

		_glCanvas.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event event) {				
				// upper-left corner coordinates
				if ( _ptUpperLeft == null ) 
					_ptUpperLeft = new Point[1];
				_ptUpperLeft[0] = _glCanvas.toDisplay(0, 0);
				// canvas bounds
				Rectangle bounds = _glCanvas.getBounds();
				// aspect ratio
				float fAspect = (float) bounds.width / (float) bounds.height;
				// set the current canvas
				_glCanvas.setCurrent();
				// use the graphic context
				try {
					GLContext.useContext(_glCanvas);
				} catch(LWJGLException exc) { exc.printStackTrace(); }
				// pass data to camera object
        		_camera.setAspectRatio( fAspect );
        		_camera.setFieldOfView( 60 );
        		_camera.applyPerspectiveMatrix();
        		_camera.applyOptimalStates();
        		// mouse grabbed
        		Mouse.setGrabbed(true);
				// GL stuff
				GL11.glViewport(0, 0, bounds.width, bounds.height);
				GL11.glMatrixMode(GL11.GL_PROJECTION);
				GL11.glLoadIdentity();
				GLU.gluPerspective(45.0f, fAspect, 0.5f, 400.0f);
				GL11.glMatrixMode(GL11.GL_MODELVIEW);
				GL11.glLoadIdentity();
				
				// log message
				MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
						"RESIZE: \n" + 
						"\t_ptUpperLeft[0].x = " + _ptUpperLeft[0].x + "\n" +
						"\t_ptUpperLeft[0].y = " + _ptUpperLeft[0].y + "\n"
						);
				
			}
		});
		
		MyOpenGLSceneGrip gripListener = new MyOpenGLSceneGrip(this); 
		_glCanvas.addMouseListener(gripListener);
		_glCanvas.addMouseMoveListener(gripListener);
		_glCanvas.addListener(SWT.MouseWheel, gripListener);
		_glCanvas.addKeyListener(gripListener);
		
		// TODO: improve mouse move operations
		// http://lwjgl.org/forum/index.php?topic=3037.0;wap2
		// http://svn.codespot.com/a/eclipselabs.org/dawb/trunk/org.dawb.fable.imageviewer/src/fable/imageviewer/views/SceneGrip.java
		
		// TODO: improve mouse logic (pan, zoom, rotation)
		// TODO: setup a reset-view button/function
		// TODO: manage keyboard events (rotation)
		// TODO: toolbar buttons with conventional view actions
		
		// to be executed at the paint event
		final Runnable run = new Runnable() {
			@Override
			public void run() {
				if (!_glCanvas.isDisposed()) {
					_glCanvas.setCurrent();
					try {
						GLContext.useContext(_glCanvas);
					} catch(LWJGLException e) { e.printStackTrace(); }
					
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
					GL11.glClearColor(_bgRed, _bgGreen, _bgBlue, _bgAlpha);
					GL11.glLoadIdentity();
					GL11.glTranslatef(0.0f, 0.0f, -10.0f);
					
					GL11.glRotatef(_camera.pitch(), 
							1.0f, // pitch
							0.0f, // yaw
							0.0f  // roll
							);
					GL11.glRotatef(_camera.yaw(), 
							0.0f, // pitch
							1.0f, // yaw
							0.0f  // roll
							);
					GL11.glRotatef(_camera.roll(), 
							0.0f, // pitch
							0.0f, // yaw
							1.0f  // roll
							);

					drawTorus(1, 1.9f + ((float) Math.sin((0.0004f * 15.0f))), 25, 25);
					
					drawReference();
					
					_glCanvas.swapBuffers();
					Display.getCurrent().asyncExec(this);
					
					// log message
//					MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
//							"RUN: " + _rotation + "\n"
//							);

				}
			}
		};
		_glCanvas.addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(Event event) {
				run.run();
			}
		});
		Display.getCurrent().asyncExec(run);
	
	}// end-of-constructor


	static void drawTorus(float r, float R, int nsides, int rings) {
		float ringDelta = 2.0f * (float) Math.PI / rings;
		float sideDelta = 2.0f * (float) Math.PI / nsides;
		float theta = 0.0f, cosTheta = 1.0f, sinTheta = 0.0f;
		
		GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
		GL11.glColor3f(0.9f, 0.9f, 0.9f);
		GL11.glLineWidth(1.0f);
		
		for (int i = rings - 1; i >= 0; i--) {
			float theta1 = theta + ringDelta;
			float cosTheta1 = (float) Math.cos(theta1);
			float sinTheta1 = (float) Math.sin(theta1);
			GL11.glBegin(GL11.GL_QUAD_STRIP);
			float phi = 0.0f;
			for (int j = nsides; j >= 0; j--) {
				phi += sideDelta;
				float cosPhi = (float) Math.cos(phi);
				float sinPhi = (float) Math.sin(phi);
				float dist = R + r * cosPhi;
				GL11.glNormal3f(cosTheta1 * cosPhi, -sinTheta1 * cosPhi, sinPhi);
				GL11.glVertex3f(cosTheta1 * dist, -sinTheta1 * dist, r * sinPhi);
				GL11.glNormal3f(cosTheta * cosPhi, -sinTheta * cosPhi, sinPhi);
				GL11.glVertex3f(cosTheta * dist, -sinTheta * dist, r * sinPhi);
			}
			GL11.glEnd();
			theta = theta1;
			cosTheta = cosTheta1;
			sinTheta = sinTheta1;
		}
	}

	static void drawReference() {
		
		GL11.glLineWidth(3.8f);
		
		//-------------------------------------------	
		// x
		GL11.glColor3f(1.0f, 0.0f, 0.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP );
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(1.0f,0.0f,0.0f);
		GL11.glEnd();
		//-------------------------------------------	
		// y
		GL11.glColor3f(0.0f, 1.0f, 0.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP );
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(0.0f,1.0f,0.0f);
		GL11.glEnd();
		//-------------------------------------------	
		// z
		GL11.glColor3f(0.0f, 0.0f, 1.0f);
		GL11.glBegin(GL11.GL_LINE_STRIP );
			GL11.glVertex3f(0.0f,0.0f,0.0f);
			GL11.glVertex3f(0.0f,0.0f,1.0f);
		GL11.glEnd();
		//-------------------------------------------
	}

	/**  
	 * Shows an error  
	 *   
	 * @param s the error to show  
	 */   
	private void showMessage(String s) {   
		MessageBox mb = new MessageBox(Display.getCurrent().getActiveShell(), SWT.ICON_WORKING | SWT.OK);   
		mb.setText("Yes!");
		mb.setMessage(s);   
		mb.open();   
	}   

	
}// end-of-class definition
