package org.moosyca.gui;

import java.io.FileInputStream;   
import java.io.IOException;   

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.*;   
import org.eclipse.swt.events.*;   
import org.eclipse.swt.graphics.Image;   
import org.eclipse.swt.widgets.*;   

/**  
 * This class contains the toolbar for the Xml View application  
 */   
public class MyXMLViewToolbar {   
	
	private static Image OPEN_IMG = getImage("Folder_32x32.png");
	private static Image CLOSE_IMG = getImage("FolderAirplane_32x32.png");
	private static Image ABOUT_IMG = getImage("XML_128x128.png");

	private ToolBar _toolBar;   

	/**  
	 * XmlViewToolbar constructor  
	 *   
	 * @param composite the parent composite  
	 */   
	public MyXMLViewToolbar(Composite composite) {   
		// Create the images used in the toolbar   
		createImages(composite);   

		// Create the toolbar   
		_toolBar = new ToolBar(composite, SWT.HORIZONTAL | SWT.FLAT);   

		// Add the items to the toolbar   
		createItems();   
	}   

	/**  
	 * Gets the underlying toolbar  
	 *   
	 * @return ToolBar  
	 */   
	public ToolBar getToolBar() {   
		return _toolBar;   
	}   

	/**  
	 * Disposes any resources  
	 */   
	public void dispose() {   
		disposeImages();   
	}   

	/**  
	 * Disposes the images  
	 */   
	private void disposeImages() {   
		if (OPEN_IMG != null) {   
			OPEN_IMG.dispose();   
			OPEN_IMG = null;   
		}   
		if (CLOSE_IMG != null) {   
			CLOSE_IMG.dispose();   
			CLOSE_IMG = null;   
		}   
		if (ABOUT_IMG != null) {   
			ABOUT_IMG.dispose();   
			ABOUT_IMG = null;   
		}   
	}   

	/**  
	 * Creates the toolbar items  
	 */   
	private void createItems() {   
		// Create the Open item   
		ToolItem item = createItemHelper(OPEN_IMG, "Open");   
		item.addSelectionListener(new SelectionAdapter() {   
			public void widgetSelected(SelectionEvent event) {   
				MyXMLViewDialog.getDialog().openFile();   
			}   
		});   

		// Create the Close item   
		item = createItemHelper(CLOSE_IMG, "Close");   
		item.addSelectionListener(new SelectionAdapter() {   
			public void widgetSelected(SelectionEvent event) {   
				MyXMLViewDialog.getDialog().closeFile();   
			}   
		});   

		// Insert a separator   
		new ToolItem(_toolBar, SWT.SEPARATOR);   

		// Create the About item   
		item = createItemHelper(ABOUT_IMG, "About");   
		item.addSelectionListener(new SelectionAdapter() {   
			public void widgetSelected(SelectionEvent event) {   
				MyXMLViewDialog.getDialog().about();   
			}   
		});   
	}   

	/**  
	 * Helper method to create a toolbar item  
	 *   
	 * @param image the image to use  
	 * @param text the text to use  
	 * @return ToolItem  
	 */   
	private ToolItem createItemHelper(Image image, String text) {   
		// Create a push button item   
		ToolItem item = new ToolItem(_toolBar, SWT.PUSH);   

		// If image loading failed, use text for the button.   
		// Otherwise, use the image for the button, and   
		// the text for the tool tip   
		if (image == null) {   
			item.setText(text);   
		} else {   
			item.setImage(image);   
			item.setToolTipText(text);   
		}   
		return item;   
	}   

	/**  
	 * Creates the images  
	 *   
	 * @param composite the parent composite  
	 */   
	private void createImages(Composite composite) {   
		Display display = composite.getDisplay();   
//		try {   
//			OPEN_IMG = new Image(display, new FileInputStream(MyXMLViewDialog.IMAGE_PATH   
//					+ "open.gif"));   
//			CLOSE_IMG = new Image(display, new FileInputStream(MyXMLViewDialog.IMAGE_PATH   
//					+ "close.gif"));   
//			ABOUT_IMG = new Image(display, new FileInputStream(MyXMLViewDialog.IMAGE_PATH   
//					+ "about.gif"));   
//		} catch (IOException e) {   
//			// If any images can't be loaded, we'll use text instead   
//			disposeImages();   
//		}   
	}
	
	// Helper Method to load the images
	private static Image getImage(String file) {
		ImageDescriptor image = 
				ImageDescriptor.createFromFile(
						MOOSyCA_GUI.class,
						MyXMLViewDialog.IMAGE_PATH + file
						);
		return image.createImage();
	} 

}  