package org.moosyca.gui;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;

public class MyTestMatlabAction extends Action
{
	private StatusLineManager _statusLineManager;
	short _triggerCount = 0;

	public MyTestMatlabAction(StatusLineManager sm)
	{
		super("Test Matlab command ...", AS_PUSH_BUTTON);

		_statusLineManager = sm;
		setToolTipText("Try a Matlab command yourself");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/MatlabReflections_32x32.png")
				);
	}

	public void run()
	{
		_triggerCount++;
		_statusLineManager.setMessage(
				"The Test Matlab command has fired. Count: " +
						_triggerCount
				);

		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The Test Matlab action has fired. Count: " +
						_triggerCount + "\n"
				);
		
		//----------------------------------------------------------------------
		// The Dialog
		// MyTestMatlabDialog
		InputDialog dialog = 
				new InputDialog( 
						Display.getCurrent().getActiveShell(),
						  "Test Matlab connection", 
						  "Try a Matlab command:", 
						  "", 
						  null 
						  );
		
		//----------------------------------------------------------------------
		// get the result
		int res = dialog.open();
		if (res == Window.OK)
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"The Test Matlab action: OK\n"
					);
			MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"VALUE: " + dialog.getValue() + "\n"
					);
			
			//========================================================
			// fir command to Matlab
			MOOSyCA_GUI.getApp().getMatlabControlManager().setTestMatlabCommand(dialog.getValue());
			MOOSyCA_GUI.getApp().getMatlabControlManager().evaluateMatlabCommand(dialog.getValue());;
		}
		else // CANCEL button pressed
		{
			MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
					"The Test Matlab action: CANCEL\n"
					);
		}

		
	}// end of run method

} // end-of-class definition
