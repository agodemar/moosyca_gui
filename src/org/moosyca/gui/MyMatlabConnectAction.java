package org.moosyca.gui;

import org.eclipse.jface.action.*;
import org.eclipse.jface.resource.*;
import org.eclipse.swt.widgets.Display;

import matlabcontrol.MatlabConnectionException;
import matlabcontrol.MatlabInvocationException;
import matlabcontrol.MatlabProxy;
import matlabcontrol.MatlabProxyFactory;
import matlabcontrol.MatlabProxyFactoryOptions;
// import matlabcontrol.PermissiveSecurityManager;



import java.util.concurrent.atomic.AtomicReference;

public class MyMatlabConnectAction extends Action
{
	//-------------------------------------------------------------------------------------
	private MyTopLevelComposite _topLevelContainer = null;
	public void setTopLevelContainer(MyTopLevelComposite comp) { _topLevelContainer = comp; }
	
	//-------------------------------------------------------------------------------------
	private MyMatlabControlManager _matlabControlManager = null;
	public void setMatlabControlManager(MyMatlabControlManager manager) { _matlabControlManager = manager; }

	//-------------------------------------------------------------------------------------
	StatusLineManager _statusLineManager;
	short _triggercount = 0;

	public MyMatlabConnectAction(StatusLineManager slm)
	{
		super("&Connect ...@Ctrl+N", AS_PUSH_BUTTON);

		_statusLineManager = slm;
		
		// TODO: these are set by setTopLevelContainer and setMatlabControlManager
		//       see if they can be passed in the constructor
		//_statusLineManager = MOOSyCA_GUI.getApp().getStatusLineManager();
		//_matlabControlManager = MOOSyCA_GUI.getApp().getMatlabControlManager();
		
		setToolTipText("Connect to Matlab engine");
		setImageDescriptor(
				ImageDescriptor.createFromFile(this.getClass(),"images/Matlab_32x32.png")
				);
		
	}
	public void run()
	{
		_triggercount++;
		_statusLineManager.setMessage(
				"The option action has fired. Count: " +
						_triggercount
				);

		MOOSyCA_GUI.getApp().getTopLevelComposite().getMessageTextWindow().append(
				"The Matlab|connect action has fired. Count: " +
						_triggercount + "\n"
				);
		
		// Modal widget
		MyMatlabConnectDialog dialog = 
				new MyMatlabConnectDialog( Display.getCurrent().getActiveShell() );
		dialog.open();

	}// end of run method

}// end of class definition
