package org.moosyca.gui;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.part.ViewPart;

public class MyProjectPane extends Composite
{
	private TreeViewer _viewerAircraft = null;
	private TreeViewer _viewerOperatingPoints = null;
	private TreeViewer _viewerAnalysis = null;
	
	private MyWorkSession _workSession = null;
	private MyOperatingPoints _operatingPoints = null;
	
	public MyProjectPane(Composite parent, int style) {
		
		super(parent, style);

//		Group group = new Group(this, SWT.NONE);
//		group.setText("Project");
		
		//-----------------------------------------------------
		// Work session
		
		// setup the aircraft
		MyAircraft aircraft = new MyAircraft("Boeing 747");
		aircraft.addAerocomponent(
				new MyAeroComponent(
						"Fuselage",
						MyAeroComponent.MAIN_BODY,
						"The fuselage")
				);
		aircraft.addAerocomponent(
				new MyAeroComponent(
						"Wing",
						MyAeroComponent.MAIN_LIFTING_SURFACE,
						"The main wing")
				);
		aircraft.addAerocomponent(
				new MyAeroComponent(
						"H.Tail",
						MyAeroComponent.HORIZONTAL_TAIL,
						"The horizontal tail")
				);
		aircraft.addAerocomponent(
				new MyAeroComponent(
						"V.Tail",
						MyAeroComponent.VERTICAL_TAIL,
						"The fin")
				);
		
		// create two operating points
		MyOperatingPoint op1 = new MyOperatingPoint("Cruise");
		op1.setMach(0.70);
		MyOperatingPoint op2 = new MyOperatingPoint("Landing");
		op1.setMach(0.25);
		
		// setup the work session
		_workSession = new MyWorkSession(aircraft);
		_workSession.addOperatingPoint(op1);
		_workSession.addOperatingPoint(op2);
		
		// create the MyOperatingPoints object
		_operatingPoints = new MyOperatingPoints(_workSession.getOperatingPoints());

		//-----------------------------------------------------
		// Treeviews
		
		this.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_GRAY));

		GridLayout theLayout = new GridLayout(1, false);
//		theLayout.verticalSpacing = 5;
//		theLayout.marginBottom = 0;
		this.setLayout(theLayout);
		
		Label aircraftLabel = new Label(this,SWT.NONE|SWT.CENTER);
		aircraftLabel.setText("Aircraft");
		aircraftLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		// The aircraft viewer
		
		_viewerAircraft = new TreeViewer(
				this, //composite
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		
		_viewerAircraft.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// providers
		//_viewerAircraft.setContentProvider(new MyAircraftContentProvider());
		_viewerAircraft.setContentProvider(
				new MyAircraftContentProvider(_workSession, aircraft)
				);
		_viewerAircraft.setLabelProvider(new MyAircraftLabelProvider());
		// Expand the tree
		_viewerAircraft.setAutoExpandLevel(2);
		// provide the input to the ContentProvider
		_viewerAircraft.setInput(aircraft); // --> getInitialInput

		// add a doubleclicklistener
		_viewerAircraft.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeViewer viewer = (TreeViewer) event.getViewer();
				IStructuredSelection thisSelection = (IStructuredSelection) event
						.getSelection();
				Object selectedNode = thisSelection.getFirstElement();
				viewer.setExpandedState(selectedNode,
						!viewer.getExpandedState(selectedNode));
			}
		});


		// The operating points viewer		

		Label opLabel = new Label(this,SWT.NONE|SWT.CENTER);
		opLabel.setText("Operating Points");
		opLabel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		_viewerOperatingPoints = new TreeViewer(
				this, // composite, 
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		_viewerOperatingPoints.getTree().setLayoutData(new GridData(GridData.FILL_BOTH));
		_viewerOperatingPoints.setContentProvider(new MyOperatingPointsContentProvider());
//		_viewerOperatingPoints.setContentProvider(
//				new MyOperatingPointsContentProvider(_workSession,aircraft)
//				);
		_viewerOperatingPoints.setLabelProvider(new MyOperatingPointsLabelProvider());
		// Expand the tree
		_viewerOperatingPoints.setAutoExpandLevel(2);
		// provide the input to the ContentProvider
		_viewerOperatingPoints.setInput(_operatingPoints);

		// add a doubleclicklistener
		_viewerOperatingPoints.addDoubleClickListener(new IDoubleClickListener() {

			@Override
			public void doubleClick(DoubleClickEvent event) {
				TreeViewer viewer = (TreeViewer) event.getViewer();
				IStructuredSelection thisSelection = (IStructuredSelection) event
						.getSelection();
				Object selectedNode = thisSelection.getFirstElement();
				viewer.setExpandedState(selectedNode,
						!viewer.getExpandedState(selectedNode));
			}
		});

		//-----------------------------------------------------
		
//		group.pack();
		pack();
		
	}

//	@Focus
//	public void setFocus() {
//		_viewerAircraft.getControl().setFocus();
//	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}