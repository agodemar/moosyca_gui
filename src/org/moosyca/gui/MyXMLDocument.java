package org.moosyca.gui;


import java.io.*;   

import org.jdom2.*;   
import org.jdom2.input.*;   

/**  
 * This class wraps an XML file  
 */   
public class MyXMLDocument {   
	private String filename;   
	private Document document;   

	/**  
	 * Constructs an XmlDocument  
	 *   
	 * @param filename the file name of the XML file  
	 */   
	public MyXMLDocument(String filename) {   
		this.filename = filename;   
	}   

	/**  
	 * Gets just the file name  
	 *   
	 * @return String  
	 */   
	public String getFilename() {   
		return filename.substring(filename.lastIndexOf(File.separator) + 1);   
	}   

	/**  
	 * Opens and parses the file  
	 *   
	 * @throws IOException if any problem opening or parsing  
	 */   
	public void open() throws IOException {   
		SAXBuilder builder = new SAXBuilder();   
		try {   
			document = builder.build(new FileInputStream(filename));   
		} catch (JDOMException e) {   
			throw new IOException(e.getMessage());   
		}   
	}   

	/**  
	 * Gets the underlying JDOM Document object  
	 *   
	 * @return Document  
	 */   
	public Document getDocument() {   
		return document;   
	}   
} 